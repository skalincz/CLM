import json
import os

from app.src.models.CKANDataset import CKANDataset
from app.src.models.NKODDataset import NKODDataset
from app.src.components.clm.MigrationVariant import MigrationVariant

import requests
import tempfile
from flask import session
from app.src.components.clm.Config import Config
from app.src.components.clm.Fetcher import Fetcher
from app.src.components.clm.JSONValidator import JSONValidator
from app.src.components.clm.MigrationVariant import MigrationVariant

MIGRATION_STEP_LIMIT = 50

class Migrator:
    lkod_url = ''
    dataset_endpoints = []
    fetcher = None
    config = None
    migration_type = None
    vatin = ''
    datasets = {}

    def __init__(self, lkod_url, ckan_url, access_token, vatin, migration_type=None, datasets=None) -> None:
        if datasets is None:
            datasets = {MigrationVariant.VALID.value: [], MigrationVariant.INVALID.value: []}

        self.config = Config(ckan_url, access_token)
        self.vatin = vatin
        self.migration_type = migration_type
        self.lkod_url = lkod_url
        self.json_validator = JSONValidator(lkod_url)
        self.license_prefill = None
        self.frequency_prefill = None
        self.ruian_prefill = ['']
        self.datasets = datasets

    def get_fetcher(self) -> Fetcher:
        if self.fetcher is None:
            self.fetcher = Fetcher(self.config)
        return self.fetcher

    def fetch_datasets(self) -> None:
        ckan_configuration = session['ckan']
        self.dataset_endpoints = []
        
        response = self.get_fetcher().get_request(
            'package_search?include_private=' + ('true' if len(self.config.access_token) else 'false') + '&rows=1000'+ (('&fq=organization:'+ckan_configuration['organization']) if 'organization' in ckan_configuration and len(ckan_configuration['organization']) else ''))
        if not response:
            return response
        for dataset in response['results']:
            self.dataset_endpoints.append(dataset['name'])
        return

    def migrate(self, form: dict, start: str = None, limit: int = None) -> bool:
        if type(self.datasets) is str:
            self.datasets = eval(self.datasets)
        self.fetch_datasets()

        datasets = self.dataset_endpoints
        if os.getenv('DEBUG'):
            print(" DEBUG: ALL DATASETS: ", datasets)

        if limit is not None:
            if os.getenv('DEBUG'):
                print(" DEBUG: DATASET MIGRATION LIMIT: ",limit)
            start_idx = 0
            if start is not None: 
                start_idx = self.dataset_endpoints.index(start)+1
    
            if os.getenv('DEBUG'):
                print(" DEBUG: DATASET MIGRATION START IDX:", start_idx)
            end_idx = start_idx + limit
            datasets = datasets[start_idx:end_idx] if len(datasets[start_idx:]) > limit else datasets[start_idx:]
            migration_status = session['migration'] if 'migration' in session else {}
            last_dataset = None
            if len(datasets) == limit:
                if os.getenv('DEBUG'):
                    print(" DEBUG: DATASET MIGRATION END IDX: ", end_idx)
                    print(" DEBUG: DATASET MIGRATION END IDX: ", end_idx-start_idx-1)
                last_dataset = datasets[end_idx-start_idx-1]
            else: 
                last_dataset = None

            migration_status.update({'last_dataset': last_dataset})
            if os.getenv('DEBUG'):
                print(" DEBUG: LAST DATASET: ", last_dataset)
            session['migration'] = migration_status
            if os.getenv('DEBUG'):
                print(" DEBUG: MIGRATION: ",session['migration'])

        if os.getenv('DEBUG'):
            print(" DEBUG: DATASETS: ", datasets)
        for dataset in datasets:
            if (self.migration_type == MigrationVariant.ALL.value) or\
                    (
                    self.migration_type != MigrationVariant.ALL.value
                    and dataset in self.datasets[self.migration_type]
                    ):
                if self.migrate_dataset(dataset, form) is False:
                    return False 
        return True

    def migrate_dataset(self, dataset, form=None) -> bool:

        if 'lkod' not in session and 'user' not in session:
            return False
        nkod_dataset = self.get_transformed_dataset(dataset, form)

        lkod_configuration = session['lkod']
        user_configuration = session['user']
        organization_configuration = session['user']

        try:
            dataset_create_response = nkod_dataset.create(lkod_configuration['url'], user_configuration, organization_configuration)
            dataset_id = dataset_create_response['id']
            session_response = requests.post('/'.join([lkod_configuration['url'], 'sessions']), data={'datasetId': dataset_id},
                                             headers={'Authorization': 'Bearer ' + user_configuration['accessToken']}).json()
            if 'id' not in session_response:
                raise requests.ConnectionError('Session error')
            session_id = session_response['id']
            nkod_dataset.update(lkod_configuration['url'], user_configuration, session_id, dataset_id)
        except requests.ConnectionError:
            return False
        return True

    def prepare_datasets(self, form=None) -> list:
        self.fetch_datasets()
        for dataset in self.dataset_endpoints:
            dataset_model = NKODDataset(self.lkod_url, self.vatin, self.fetch_old_dataset(dataset), form)
            self.push_to_array(dataset, dataset_model.validate())
        return self.datasets

    def get_transformed_dataset(self, dataset_name, form=None) -> NKODDataset:
        ckan_dataset = self.fetch_old_dataset(dataset_name)
        return NKODDataset(self.lkod_url, self.vatin, ckan_dataset, form)

    def fetch_old_dataset(self, dataset) -> CKANDataset:
        ckan_dataset = CKANDataset(self.get_fetcher(), dataset)
        return ckan_dataset

    def push_to_array(self, dataset, status) -> None:
        if status is True:
            self.datasets[MigrationVariant.VALID.value].append(dataset)
        elif status is False:
            self.datasets[MigrationVariant.INVALID.value].append(dataset)
