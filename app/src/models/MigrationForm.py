from flask import session
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, HiddenField, BooleanField
from wtforms.validators import DataRequired
from app.src.models.Migrator import Migrator, MIGRATION_STEP_LIMIT
from app.src.components.clm.MigrationVariant import MigrationVariant

class MigrationForm(FlaskForm):
    migrator = None
    datasets = HiddenField('Seznam datových sad', validators=[DataRequired()])
    variant = SelectField('Varianta', validators=[DataRequired()], choices=[(variant.value, variant.label()) for variant in MigrationVariant], default=MigrationVariant.ALL)
    prefill_frequency_check = BooleanField('Předvyplnit frekvenci',default=0)
    prefill_frequency = SelectField('Hodnota frekvence', validators=[DataRequired()], choices=[(0, 'Žádná'), ('irreg', 'Občasná aktualizace'),('never', 'Nikdy neaktualizováno')], default=0)
    prefill_ruian_check = BooleanField('Předvyplnit frekvenci',default=0)
    prefill_ruian = SelectField('Hodnota RÚIAN', validators=[DataRequired()], choices=[(0, 'Žádná'), ('1', 'Celá ČR')], default=0)
    prefill_license_check = BooleanField('Předvyplnit frekvenci',default=0)
    prefill_license = SelectField('Licence', validators=[DataRequired()], choices=[('none', 'Žádná'), ('cc4', 'CC4')], default=0)
    migration_form_submit = SubmitField('Spustit migraci')

    def process_data(self) -> bool:
        settings = {'variant': self.variant.data, 'prefill_frequency_check': self.prefill_frequency_check.data, 'prefill_frequency': self.prefill_frequency.data, 'prefill_ruian': self.prefill_ruian.data, 'prefill_ruian_check': self.prefill_frequency_check.data, 'prefill_license_check': self.prefill_license_check.data, 'prefill_license': self.prefill_license.data}
        if 'settings' in session and session['settings'] is not None:
            session['settings'].update(settings)
        else:
            session['settings'] = settings
        if 'lkod' not in session or 'ckan' not in session or 'user' not in session:
            return False
        return True
        

    def get_migration_datasets(self) -> list:
        return self.migrator.datasets if self.migrator is not None else []

