FROM python:3.11.4-slim-bookworm

COPY requirements.txt requirements.txt

WORKDIR /app

COPY requirements.txt main.py /

RUN pip3 install -r /requirements.txt && \
    groupadd --system -g 1001 nonroot && \
    useradd --system --base-dir /app --uid 1001 --gid nonroot nonroot && \
    chown -R nonroot /app

COPY --chown=nonroot:nonroot app/ ./

USER nonroot

EXPOSE 5000

CMD ["python3", "/main.py"]
